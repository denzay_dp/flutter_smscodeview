import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Sample for material.RaisedButton',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: RootForm(),
    );
  }
}

class RootForm extends StatefulWidget {
  @override
  RootFormState createState() => RootFormState();
}

class RootFormState extends State<RootForm> {
  var _focusedIndex = 0;
  var _codeLength = 3;

  // Define the focus node. To manage the lifecycle, create the FocusNode in
  // the initState method, and clean it up in the dispose method
  List<FocusNode> _focuses = new List();
  List<Widget> _inputWidgets = new List();

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < _codeLength; i++) {
      _focuses.add(FocusNode());

//      _inputWidgets.add(Container(
//          width: 40,
//          margin: EdgeInsets.all(5),
//          child: RawKeyboardListener(
//            focusNode: _focuses[i],
//            onKey: _handleKeyEvent,
//            child: TextField(
//              keyboardType: TextInputType.number,
//              focusNode: _focuses[i],
//              maxLength: 1,
//              maxLengthEnforced: false,
//              decoration: InputDecoration(
//                  border: OutlineInputBorder(
//                    borderRadius: BorderRadius.circular(5.0),
//                  )),
//              autofocus: false,
//            ),
//          )));
    }
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed
    for (int i = 0; i < _codeLength; i++)
      _focuses[i].dispose();
    super.dispose();
  }

  void _processFocus() {
    FocusScope.of(context).requestFocus(_focuses[_focusedIndex]);
  }

  void _handleKeyEvent(RawKeyEvent event) {
    // RawKeyUpEvent - doesn't trigger with backspace
    //  setState(() {
    if (event is RawKeyDownEvent) {
      if (event.logicalKey == LogicalKeyboardKey.arrowLeft ||
          event.logicalKey == LogicalKeyboardKey.backspace) {
        if (_focusedIndex > 0) {
          _focusedIndex--;
          _processFocus();
        }
      } else if (event.logicalKey == LogicalKeyboardKey.arrowRight) {
        if (_focusedIndex < _codeLength - 1)
          _focusedIndex++;
        _processFocus();
      }
    }
  }

//  });


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            // ToDo: как-то передать весь список
//          children: <Widget>[
//            _inputWidgets[0], _inputWidgets[1], _inputWidgets[2] ],
            children: <Widget>[
              Container(
                  width: 40,
                  margin: EdgeInsets.all(5),
                  child: RawKeyboardListener(
                    focusNode: _focuses[0],
                    onKey: _handleKeyEvent,
                    child: TextField(
                      keyboardType: TextInputType.number,
                      focusNode: _focuses[0],
                      maxLength: 1,
                      decoration: InputDecoration(
                          labelText: '1',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          )),
                      autofocus: true,
                    ),
                  )),
              Container(
                  width: 40,
                  margin: EdgeInsets.all(5),
                  child: RawKeyboardListener(
                    focusNode: _focuses[1],
                    onKey: _handleKeyEvent,
                    child: TextField(
                      focusNode: _focuses[1],
                      keyboardType: TextInputType.number,
                      maxLength: 1,
                      decoration: InputDecoration(
                          labelText: '2',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          )),
                    ),
                  )),
              Container(
                width: 40,
                margin: EdgeInsets.all(5),
                child: RawKeyboardListener(
                  focusNode: _focuses[2],
                  onKey: _handleKeyEvent,
                  child: TextField(
                    focusNode: _focuses[2],
                    keyboardType: TextInputType.number,
                    maxLength: 1,
                    decoration: InputDecoration(
                      labelText: '3',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
    );
  }
}
